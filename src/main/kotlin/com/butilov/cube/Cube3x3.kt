package com.butilov.cube

import com.butilov.cube.CubeOperation.*

class Cube3x3 {

    private val list = listOf(
        angleOYG, pairOY, angleOYB, pairYG, centerY, pairYB, angleRYG, pairRY, angleRYB,
        pairOG, centerO, pairOB, centerG, pairRG, centerB, pairRB, centerR,
        angleOWG, pairOW, angleOWB, pairWG, centerW, pairWB, angleRWG, pairRW, angleRWB
    )
    private val rotator: CoordinateRotator = CoordinateRotator()

    init {
        applyOps(F2, S2, B2, U, Eh, Dh) // green center, white top
    }

    fun applyOps(vararg ops: CubeOperation) {
        val cubeClass = javaClass
        ops.forEach {
            val method = cubeClass.getMethod(it.name)
            method.invoke(this)
        }
    }

    fun getUpperLayer() = list.filter { it.coordinate.y == 1 }

    fun getDownLayer() = list.filter { it.coordinate.y == -1 }

    fun getRightLayer() = list.filter { it.coordinate.z == 1 }

    fun getLeftLayer() = list.filter { it.coordinate.z == -1 }

    fun getFrontLayer() = list.filter { it.coordinate.x == 1 }

    fun getBackLayer() = list.filter { it.coordinate.x == -1 }

    fun getSLayer() = list.filter { it.coordinate.x == 0 }

    fun getMLayer() = list.filter { it.coordinate.z == 0 }

    fun getELayer() = list.filter { it.coordinate.y == 0 }

    fun R() {
        val right = getRightLayer()
        right.forEach(Element::swapColorXY)
        right.forEach { rotator.aroundZminus90(it.coordinate) }
    }

    fun L() {
        val left = getLeftLayer()
        left.forEach(Element::swapColorXY)
        left.forEach { rotator.aroundZ90(it.coordinate) }
    }

    fun F() {
        val front = getFrontLayer()
        front.forEach(Element::swapColorYZ)
        front.forEach { rotator.aroundX90(it.coordinate) }
    }

    fun B() {
        val back = getBackLayer()
        back.forEach(Element::swapColorYZ)
        back.forEach { rotator.aroundXminus90(it.coordinate) }
    }

    fun U() {
        val upper = getUpperLayer()
        upper.forEach(Element::swapColorXZ)
        upper.forEach { rotator.aroundY90(it.coordinate) }
    }

    fun D() {
        val down = getDownLayer()
        down.forEach(Element::swapColorXZ)
        down.forEach { rotator.aroundYminus90(it.coordinate) }
    }

    fun S() {
        val s = getSLayer()
        s.forEach(Element::swapColorYZ)
        s.forEach { rotator.aroundX90(it.coordinate) }
    }

    fun M() {
        val middle = getMLayer()
        middle.forEach(Element::swapColorXY)
        middle.forEach { rotator.aroundZ90(it.coordinate) }
    }

    fun E() {
        val e = getELayer()
        e.forEach(Element::swapColorXZ)
        e.forEach { rotator.aroundYminus90(it.coordinate) }
    }

    // R', L', F', B', U', D', S', M', E'

    fun Rh() {
        val right = getRightLayer()
        right.forEach(Element::swapColorXY)
        right.forEach { rotator.aroundZ90(it.coordinate) }
    }

    fun Lh() {
        val left = getLeftLayer()
        left.forEach(Element::swapColorXY)
        left.forEach { rotator.aroundZminus90(it.coordinate) }
    }

    fun Fh() {
        val front = getFrontLayer()
        front.forEach(Element::swapColorYZ)
        front.forEach { rotator.aroundXminus90(it.coordinate) }
    }

    fun Bh() {
        val back = getBackLayer()
        back.forEach(Element::swapColorYZ)
        back.forEach { e -> rotator.aroundX90(e.coordinate) }
    }

    fun Uh() {
        val upper = getUpperLayer()
        upper.forEach(Element::swapColorXZ)
        upper.forEach { rotator.aroundYminus90(it.coordinate) }
    }

    fun Dh() {
        val down = getDownLayer()
        down.forEach(Element::swapColorXZ)
        down.forEach { rotator.aroundY90(it.coordinate) }
    }

    fun Sh() {
        val down = getSLayer()
        down.forEach(Element::swapColorYZ)
        down.forEach { rotator.aroundXminus90(it.coordinate) }
    }

    fun Mh() {
        val middle = getMLayer()
        middle.forEach(Element::swapColorXY)
        middle.forEach { rotator.aroundZminus90(it.coordinate) }
    }

    fun Eh() {
        val e = getELayer()
        e.forEach(Element::swapColorXZ)
        e.forEach { rotator.aroundY90(it.coordinate) }
    }

    fun R2() = getRightLayer().forEach { rotator.aroundZ180(it.coordinate) }

    fun L2() = getLeftLayer().forEach { rotator.aroundZ180(it.coordinate) }

    fun F2() = getFrontLayer().forEach { rotator.aroundX180(it.coordinate) }

    fun B2() = getBackLayer().forEach { rotator.aroundX180(it.coordinate) }

    fun U2() = getUpperLayer().forEach { rotator.aroundY180(it.coordinate) }

    fun D2() = getDownLayer().forEach { rotator.aroundY180(it.coordinate) }

    fun S2() = getSLayer().forEach { rotator.aroundX180(it.coordinate) }

    fun M2() = getMLayer().forEach { rotator.aroundZ180(it.coordinate) }

    fun E2()  = getELayer().forEach { rotator.aroundY180(it.coordinate) }

    fun y() {
        list.forEach(Element::swapColorXZ)
        list.forEach { rotator.aroundY90(it.coordinate) }
    }

    fun yh() {
        list.forEach(Element::swapColorXZ)
        list.forEach { rotator.aroundYminus90(it.coordinate) }
    }

    fun x() {
        list.forEach(Element::swapColorXY)
        list.forEach { rotator.aroundZminus90(it.coordinate) }
    }

    fun xh() {
        list.forEach(Element::swapColorXY)
        list.forEach { rotator.aroundZ90(it.coordinate) }
    }

    fun z() {
        list.forEach(Element::swapColorYZ)
        list.forEach { rotator.aroundX90(it.coordinate) }
    }

    fun zh() {
        list.forEach(Element::swapColorYZ)
        list.forEach { rotator.aroundXminus90(it.coordinate) }
    }

}
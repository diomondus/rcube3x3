package com.butilov.cube

import com.butilov.cube.Color.*

enum class Color {
    Red, Green, Blue, Yellow, White, Orange, NotUsed;

    override fun toString() = name.substring(0, 1)
}

class Coordinate(
    var x: Int, var y: Int, var z: Int
)

/**
 *       y |
 *         |
 *         |__ __ __          Y
 *        /        z        G O B R
 *       /                    W
 *    x /                     R
 */
class Element(
    var xColor: Color,
    var yColor: Color,
    var zColor: Color,
    var coordinate: Coordinate
) {
    val original: Coordinate = coordinate

    fun swapColorXY() {
        val buf = xColor
        xColor = yColor
        yColor = buf
    }

    fun swapColorXZ() {
        val buf = xColor
        xColor = zColor
        zColor = buf
    }

    fun swapColorYZ() {
        val buf = yColor
        yColor = zColor
        zColor = buf
    }
}

// Upper layer
val angleOYG = Element(Orange, Yellow, Green, Coordinate(1, 1, -1))
val pairOY = Element(Orange, Yellow, NotUsed, Coordinate(1, 1, 0))
val angleOYB = Element(Orange, Yellow, Blue, Coordinate(1, 1, 1))

val pairYG = Element(NotUsed, Yellow, Green, Coordinate(0, 1, -1))
val centerY = Element(NotUsed, Yellow, NotUsed, Coordinate(0, 1, 0))
val pairYB = Element(NotUsed, Yellow, Blue, Coordinate(0, 1, 1))

val angleRYG = Element(Red, Yellow, Green, Coordinate(-1, 1, -1))
val pairRY = Element(Red, Yellow, NotUsed, Coordinate(-1, 1, 0))
val angleRYB = Element(Red, Yellow, Blue, Coordinate(-1, 1, 1))

// Middle layer
val pairOG = Element(Orange, NotUsed, Green, Coordinate(1, 0, -1))
val centerO = Element(Orange, NotUsed, NotUsed, Coordinate(1, 0, 0))
val pairOB = Element(Orange, NotUsed, Blue, Coordinate(1, 0, 1))
val centerG = Element(NotUsed, NotUsed, Green, Coordinate(0, 0, -1))
val pairRG = Element(Red, NotUsed, Green, Coordinate(-1, 0, -1))
val centerB = Element(NotUsed, NotUsed, Blue, Coordinate(0, 0, 1))
val pairRB = Element(Red, NotUsed, Blue, Coordinate(-1, 0, 1))
val centerR = Element(Red, NotUsed, NotUsed, Coordinate(-1, 0, 0))

// Bottom layer
val angleOWG = Element(Orange, White, Green, Coordinate(1, -1, -1))
val pairOW = Element(Orange, White, NotUsed, Coordinate(1, -1, 0))
val angleOWB = Element(Orange, White, Blue, Coordinate(1, -1, 1))

val pairWG = Element(NotUsed, White, Green, Coordinate(0, -1, -1))
val centerW = Element(NotUsed, White, NotUsed, Coordinate(0, -1, 0))
val pairWB = Element(NotUsed, White, Blue, Coordinate(0, -1, 1))

val angleRWG = Element(Red, White, Green, Coordinate(-1, -1, -1))
val pairRW = Element(Red, White, NotUsed, Coordinate(-1, -1, 0))
val angleRWB = Element(Red, White, Blue, Coordinate(-1, -1, 1))

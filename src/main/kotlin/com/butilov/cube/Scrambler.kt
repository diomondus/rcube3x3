package com.butilov.cube

import com.butilov.cube.CubeOperation.*
import java.lang.System.nanoTime
import kotlin.random.Random

class Scrambler {

    private val operations = arrayOf(R, L, F, B, U, D, Rh, Lh, Fh, Bh, Uh, Dh, R2, L2, F2, B2, U2, D2)
    private val opsCount = operations.size
    private val opsCountPerFringe = opsCount / 3

    fun scramble(steps: Int = 20, random: Random = Random(nanoTime())): Array<CubeOperation> {
        val randomInts = getRandomOpsIdList(random, steps)
        return randomInts.map { operations[it] }.toTypedArray()
    }

    fun niceDisplay(listOps: Array<CubeOperation>): String {
        return listOps.joinToString(" ").replace("h", "'")
    }

    private fun getRandomOpsIdList(random: Random, steps: Int): List<Int> {
        val randomInts = mutableListOf(random.next())

        while (randomInts.size != steps) {
            val nextInt = random.next()
            if (randomInts.last() % opsCountPerFringe == nextInt % opsCountPerFringe) {
                continue
            } else {
                randomInts.add(nextInt)
            }
        }
        return randomInts
    }

    private fun Random.next() = nextInt(0, opsCount)

}

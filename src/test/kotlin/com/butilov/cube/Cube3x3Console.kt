package com.butilov.cube

import com.butilov.cube.Color.*
import com.butilov.cube.CubeOperation.*
import org.junit.Test

class Cube3x3Console {

    val INDENT = "     "
    val ANSI_RED_BG = "\u001b[48;5;196m"
    val ANSI_GREEN_BG = "\u001b[48;5;2m"
    val ANSI_YELLOW_BG = "\u001b[48;5;226m"
    val ANSI_BLUE_BG = "\u001b[48;5;27m"
    val ANSI_WHITE_BG = "\u001b[48;5;255m"
    val ANSI_ORANGE_BG = "\u001b[48;5;208m"
    val ANSI_RESET_BG = "\u001b[0m"

    val map = mapOf(
        Red to ANSI_RED_BG,
        Green to ANSI_GREEN_BG,
        Blue to ANSI_BLUE_BG,
        Yellow to ANSI_YELLOW_BG,
        White to ANSI_WHITE_BG,
        Orange to ANSI_ORANGE_BG
    )

    private fun paint(cube: Cube3x3) {
        val u = cube.getUpperLayer()
        val l = cube.getLeftLayer()
        val f = cube.getFrontLayer()
        val r = cube.getRightLayer()
        val b = cube.getBackLayer()
        val d = cube.getDownLayer()

        println()
        print(INDENT)
        with(u.first { with(it.coordinate) { x == -1 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(u.first { with(it.coordinate) { x == -1 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(u.first { with(it.coordinate) { x == -1 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(u.first { with(it.coordinate) { x == 0 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(u.first { with(it.coordinate) { x == 0 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(u.first { with(it.coordinate) { x == 0 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(u.first { with(it.coordinate) { x == 1 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(u.first { with(it.coordinate) { x == 1 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(u.first { with(it.coordinate) { x == 1 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)

        with(l.first { with(it.coordinate) { y == 1 && x == -1 } }.zColor) { print("${map[this]}$this ") }
        with(l.first { with(it.coordinate) { y == 1 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(l.first { with(it.coordinate) { y == 1 && x == 1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(f.first { with(it.coordinate) { y == 1 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(f.first { with(it.coordinate) { y == 1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(f.first { with(it.coordinate) { y == 1 && z == 1 } }.xColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(r.first { with(it.coordinate) { y == 1 && x == 1 } }.zColor) { print("${map[this]}$this ") }
        with(r.first { with(it.coordinate) { y == 1 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(r.first { with(it.coordinate) { y == 1 && x == -1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(b.first { with(it.coordinate) { y == 1 && z == 1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == 1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == 1 && z == -1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)

        with(l.first { with(it.coordinate) { y == 0 && x == -1 } }.zColor) { print("${map[this]}$this ") }
        with(l.first { with(it.coordinate) { y == 0 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(l.first { with(it.coordinate) { y == 0 && x == 1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(f.first { with(it.coordinate) { y == 0 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(f.first { with(it.coordinate) { y == 0 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(f.first { with(it.coordinate) { y == 0 && z == 1 } }.xColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(r.first { with(it.coordinate) { y == 0 && x == 1 } }.zColor) { print("${map[this]}$this ") }
        with(r.first { with(it.coordinate) { y == 0 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(r.first { with(it.coordinate) { y == 0 && x == -1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(b.first { with(it.coordinate) { y == 0 && z == 1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == 0 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == 0 && z == -1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)

        with(l.first { with(it.coordinate) { y == -1 && x == -1 } }.zColor) { print("${map[this]}$this ") }
        with(l.first { with(it.coordinate) { y == -1 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(l.first { with(it.coordinate) { y == -1 && x == 1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(f.first { with(it.coordinate) { y == -1 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(f.first { with(it.coordinate) { y == -1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(f.first { with(it.coordinate) { y == -1 && z == 1 } }.xColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(r.first { with(it.coordinate) { y == -1 && x == 1 } }.zColor) { print("${map[this]}$this ") }
        with(r.first { with(it.coordinate) { y == -1 && x == 0 } }.zColor) { print("${map[this]}$this") }
        with(r.first { with(it.coordinate) { y == -1 && x == -1 } }.zColor) { print("${map[this]} $this") }
        print(ANSI_RESET_BG)
        with(b.first { with(it.coordinate) { y == -1 && z == 1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == -1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == -1 && z == -1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)

        print(INDENT)
        with(d.first { with(it.coordinate) { x == 1 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(d.first { with(it.coordinate) { x == 1 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(d.first { with(it.coordinate) { x == 1 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(d.first { with(it.coordinate) { x == 0 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(d.first { with(it.coordinate) { x == 0 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(d.first { with(it.coordinate) { x == 0 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(d.first { with(it.coordinate) { x == -1 && z == -1 } }.yColor) { print("${map[this]}$this ") }
        with(d.first { with(it.coordinate) { x == -1 && z == 0 } }.yColor) { print("${map[this]}$this") }
        with(d.first { with(it.coordinate) { x == -1 && z == 1 } }.yColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)

        print(INDENT)
        with(b.first { with(it.coordinate) { y == 1 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == 1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == 1 && z == 1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(b.first { with(it.coordinate) { y == 0 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == 0 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == 0 && z == 1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
        print(INDENT)
        with(b.first { with(it.coordinate) { y == -1 && z == -1 } }.xColor) { print("${map[this]}$this ") }
        with(b.first { with(it.coordinate) { y == -1 && z == 0 } }.xColor) { print("${map[this]}$this") }
        with(b.first { with(it.coordinate) { y == -1 && z == 1 } }.xColor) { print("${map[this]} $this") }
        println(ANSI_RESET_BG)
    }

    @Test
    fun test() {
        val cube = Cube3x3()
        paint(cube)
    }

    @Test
    fun scramble() {
        val cube = Cube3x3()
        val scrambler = Scrambler()

        val randomOperations = scrambler.scramble()
        cube.applyOps(*randomOperations)

        println(scrambler.niceDisplay(randomOperations))
        paint(cube)
    }

    @Test
    fun `draw snowflake on top layer`() {
        val cube = Cube3x3()

        cube.applyOps(S, Rh, Uh, R, U, R, U, R, Uh, Rh, Sh)

        paint(cube)
    }
}